# Схема реализации инфраструктуры

![image info](assets/IMG_0101.drawio.png)

## Сборка


### Отправить проект в новый репозиторий

git add .

git commit -m ‘Initializing the Project’

git push origin main

В новом репозитории запустится сборка инфраструктуры. 

В веб-приложении управления репозиториями перейти  my group -> my project -> Build -> Pipelines

Дождаться выполнения первого и второго этапа. 


### Перейти в терминал

В директории .ssh создать файл GB-D.pem c закрытым ключом ключевой пары GB-D. 

Присвоить файлу GB-D.pem необходимые права. 

chmod 400 ~/.ssh/GB-D.pem 

Соединиться с сервером мониторинга с пробросом сокета агента. 

eval `ssh-agent`

ssh-add  .ssh/GB-D.pem 

ssh -A -t -L 8086:127.0.0.1:8086 ubuntu@89.208.197.161 ssh -A -t -L 8086:127.0.0.1:8086 ubuntu@10.10.10.14 


### Войти в приложение influxdb

http://127.0.0.1:8086/

get started ->

ввести имя пользователя … 

inicial organization name = org

bucket name = telegraf

нажать load data -> telegraf -> create configuration

выбрать bucket telegraf

в поиске набрать system

выбрать system

continuo configure

в name configuration набрать telegraf

save and test

нажать  generate api tocken -> custom api tocken 

выбрать buckets ->  telegraf и указать write

Копировать токен 


### Добавить переменную с токеном в проект 
-> my group -> my project -> Settings -> ci/cd -> Variables. 

установить атрибут ‘Mask variable’ при создании переменной. Атрибуты ‘Protect variable’ и ‘Expand variable reference’ оставить пустыми

GB_D_TELEGRAF_TOKEN  - Вставить токен


### Запустить оставшийся этап

В веб-приложении управления репозиториями перейти  my group -> my project -> Build -> Pipelines и запустить оставшийся этап. 

Доступ к Grafana

eval `ssh-agent`

ssh-add  .ssh/GB-D.pem 

ssh -A -t -L 3000:127.0.0.1:3000 ubuntu@89.208.197.161 ssh -A -t -L 3000:127.0.0.1:3000 ubuntu@10.10.10.14



### Войти в приложение influxdb

http://127.0.0.1:8086/

переходим в data explorer

выбираем telegraf system и новые графики станут доступны




## Создать проект

Создать проект в https://msk.cloud.vk.com/

получить 1 плавающий IP адрес

Создать ключевую пару с именем GB-D

### Создание инфраструктуры.

### Добавить переменные
https://gitlab.com/ -> my group -> my project -> Settings -> ci/cd -> Variables.

установить атрибут ‘Mask variable’ при создании переменной. Атрибуты ‘Protect variable’ и ‘Expand variable reference’ оставить пустыми.

GB_D_ANSIBLE_WORDPRESS_DIRNAME  - имя директории Wordpress приложения

GB_D_DB_DATABASE_NAME  - имя базы данных

GB_D_DB_HOST  - 10.10.10.15

GB_D_DB_USERNAME  - имя пользователя базы данных

GB_D_DB_USER_PASSWORD  - пароль пользователя базы данных

GB_D_VKCS_BASTION_IP  - полученный плавающий IP 

GB_D_VKCS_PASSWORD  - пароль https://msk.cloud.vk.com/

GB_D_VKCS_PROJECT_ID  - ID проекта https://msk.cloud.vk.com/

GB_D_VKCS_USERNAME  - имя пользователя https://msk.cloud.vk.com/


### Добавить файлы

https://gitlab.com/ -> my group -> my project -> Settings -> ci/cd -> Secure Files

GB_D_ANSIBLE_PATH_TO_SSH_KEY  -  /builds/<my group>/<my project>/ansible/.s/GB-D.pem

GB_D_ANSIBLE_USER   - ubuntu

GB_D_ANSIBLE_BASTION_IP   - полученный плавающий IP  

GB-D.pem   -  полученный секретный ключ ключевой пары GB-D  


### Клонировать проект

git clone https://gitlab.com/gb2783547/gbd.git 

Перейти в директорию с проектом и удалить .git каталог.

Создать свой новый базовый проект в системе управления репозиториями программного кода с README.md файлом. В README.md будет инструкция для клонирования и актикации проекта. Выполнить её. 

В каталоге с новым базовым проектом удалить файл README.md. 

Копировать все файлы и каталоги, включая скрытые файлы начинающиеся с точки, из директории с готовым проектом в директорию с созданным новым базовым проектом. 


### Отправить проект в новый репозиторий

git add .

git commit -m ‘Initializing the Project’

git push origin main

В новом репозитории запустится сборка инфраструктуры. 

В веб-приложении управления репозиториями перейти  my group -> my project -> Build -> Pipelines

Дождаться выполнения первого и второго этапа. 


### Перейти в терминал

В директории .ssh создать файл GB-D.pem c закрытым ключом ключевой пары GB-D. 

Присвоить файлу GB-D.pem необходимые права. 

chmod 400 ~/.ssh/GB-D.pem 

Соединиться с сервером мониторинга с пробросом сокета агента. 

eval `ssh-agent`

ssh-add  .ssh/GB-D.pem 

ssh -A -t -L 8086:127.0.0.1:8086 ubuntu@89.208.197.161 ssh -A -t -L 8086:127.0.0.1:8086 ubuntu@10.10.10.14 


### Войти в приложение influxdb

http://127.0.0.1:8086/

get started ->

ввести имя пользователя … 

inicial organization name = org

bucket name = telegraf

нажать load data -> telegraf -> create configuration

выбрать bucket telegraf

в поиске набрать system

выбрать system

continuo configure

в name configuration набрать telegraf

save and test

нажать  generate api tocken -> custom api tocken 

выбрать buckets ->  telegraf и указать write

Копировать токен 


### Добавить переменную с токеном в проект 
-> my group -> my project -> Settings -> ci/cd -> Variables. 

установить атрибут ‘Mask variable’ при создании переменной. Атрибуты ‘Protect variable’ и ‘Expand variable reference’ оставить пустыми

GB_D_TELEGRAF_TOKEN  - Вставить токен


### Запустить оставшийся этап

В веб-приложении управления репозиториями перейти  my group -> my project -> Build -> Pipelines и запустить оставшийся этап. 

Доступ к Grafana

eval `ssh-agent`

ssh-add  .ssh/GB-D.pem 

ssh -A -t -L 3000:127.0.0.1:3000 ubuntu@89.208.197.161 ssh -A -t -L 3000:127.0.0.1:3000 ubuntu@10.10.10.14



### Войти в приложение influxdb

http://127.0.0.1:8086/

переходим в data explorer

выбираем telegraf system и новые графики станут доступны

