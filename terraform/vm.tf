data "vkcs_images_image" "compute" {
   name = "${var.image_flavor}"
}
data "vkcs_compute_flavor" "compute" {
   name = "${var.compute_flavor}"
}

resource "vkcs_compute_instance" "compute_1" {
  name              = "gb_vm1"
  flavor_id         = "${data.vkcs_compute_flavor.compute.id}"
  image_id          = "${data.vkcs_images_image.compute.id}"
  security_groups   = ["default", "ssh+www"]
  key_pair          = "${var.key_pairs.vm}"
  availability_zone = "${var.availability_zones.vm1}"
 
  network {
    uuid        = vkcs_networking_network.net.id
    fixed_ip_v4 = "10.10.10.11"
  }

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  depends_on = [
    vkcs_networking_network.net,
    vkcs_networking_subnet.snet
  ]
}


resource "vkcs_compute_instance" "compute_2" {
  name              = "gb_vm2"
  flavor_id         = "${data.vkcs_compute_flavor.compute.id}"
  image_id          = "${data.vkcs_images_image.compute.id}"
  security_groups   = ["default", "ssh+www"]
  key_pair          = "${var.key_pairs.vm}"
  availability_zone = "${var.availability_zones.vm1}"

  network {
    uuid        = vkcs_networking_network.net.id
    fixed_ip_v4 = "10.10.10.12"
  }

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  depends_on = [
    vkcs_networking_network.net,
    vkcs_networking_subnet.snet
  ]
}


resource "vkcs_compute_instance" "compute_3" {
  name              = "gb_mon"
  flavor_id         = "${data.vkcs_compute_flavor.compute.id}"
  image_id          = "${data.vkcs_images_image.compute.id}"
  security_groups   = ["default", "ssh+www"]
  key_pair          = "${var.key_pairs.vm}"
  availability_zone = "${var.availability_zones.vm1}"

  network {
    uuid        = vkcs_networking_network.net.id
    fixed_ip_v4 = "10.10.10.14"
  }

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  depends_on = [
    vkcs_networking_network.net,
    vkcs_networking_subnet.snet
  ]
}



resource "vkcs_compute_instance" "compute_4" {
  name              = "gb_vm4"
  flavor_id         = "${data.vkcs_compute_flavor.compute.id}"
  image_id          = "${data.vkcs_images_image.compute.id}"
  security_groups   = ["default", "ssh", "ssh+www"]
  key_pair          = "${var.key_pairs.vm}"
  availability_zone = "${var.availability_zones.vm1}"

  network {
    uuid        = vkcs_networking_network.net.id
    fixed_ip_v4 = "10.10.10.98"
    #access_network = true
  }
  #network {
  #  port   = vkcs_networking_port.port.id
  #}


  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-ssd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  depends_on = [
    vkcs_networking_router_interface.db,
    vkcs_networking_network.net,
    vkcs_networking_subnet.snet
  ]
}

resource "vkcs_networking_floatingip" "fip_2" {
  pool = "ext-net"
}

resource "vkcs_compute_floatingip_associate" "fip_2" {
  floating_ip = "${var.float_ip}"
  instance_id = vkcs_compute_instance.compute_4.id

}

