variable "image_flavor" {
  type = string
  default = "Ubuntu-20.04.1-202008"
}

variable "compute_flavor" {
  type = string
  default = "Basic-1-2-20"
}

variable "key_pairs" {
  type = map
  default = {
      vm1 = "gb-vm1-GZ1"
      vm2 = "gb-vm2-MS1"
      vm  =  "GB-D"
    }
}

variable "availability_zones" {
  type = map 
  default = {
      vm2 = "GZ1"
      vm1 = "MS1"
    }
}

variable "db-instance-flavor" {
  type = string
  default = "Standard-2-8-50"
}

variable "db_user_password" {
  type       = string
  sensitive = true
}

variable "db_username" {
  type       = string
  sensitive = true
}

variable "db_database" {
  type       = string
  sensitive = true
}

variable "vkcs_username" {
  type       = string
  sensitive = true
}

variable "vkcs_password" {
  type       = string
  sensitive = true
}

variable "vkcs_project_id" {
  type       = string
  sensitive = true
}

variable "float_ip" {
  type       = string
}

#variable "gb_d_compute_pem" {
#  type       = string
#  sensitive = true
#}
