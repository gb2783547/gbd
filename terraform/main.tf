data "vkcs_networking_network" "extnet" {
   name = "ext-net"
}

resource "vkcs_networking_network" "net" {  
  name = "gb_net"
}

resource "vkcs_networking_subnet" "snet" {
  name            = "gb_subnet"
  network_id      = "${vkcs_networking_network.net.id}" 
  cidr            = "10.10.10.0/24"
  gateway_ip      = "10.10.10.1"
  dns_nameservers = ["10.10.10.2", "10.10.10.3"]
}

resource "vkcs_networking_router" "router" {
   name                = "router_gb"
   admin_state_up      = true
   external_network_id = "${data.vkcs_networking_network.extnet.id}" 
}

resource "vkcs_networking_router_interface" "db" {
   router_id = "${vkcs_networking_router.router.id}" 
   subnet_id = "${vkcs_networking_subnet.snet.id}"  
}
