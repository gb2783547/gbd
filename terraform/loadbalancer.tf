resource "vkcs_lb_loadbalancer" "lb" {
  name          = "gb_lb"
  vip_subnet_id = "${vkcs_networking_subnet.snet.id}"
  vip_address   = "10.10.10.100"
}

resource "vkcs_lb_listener" "listener" {
  name            = "gb_listener"
  protocol        = "HTTP"
  protocol_port   = 80
  loadbalancer_id = "${vkcs_lb_loadbalancer.lb.id}"
}

resource "vkcs_lb_pool" "pool" {
  name        = "pool"
  protocol    = "HTTP"
  lb_method   = "ROUND_ROBIN"
  listener_id = "${vkcs_lb_listener.listener.id}"
}

resource "vkcs_lb_member" "member_1" {
  address       = "10.10.10.11"
  protocol_port = 80
  pool_id       = "${vkcs_lb_pool.pool.id}"
  subnet_id     = "${vkcs_networking_subnet.snet.id}"
}

resource "vkcs_lb_member" "member_2" {
  address       = "10.10.10.12"
  protocol_port = 80
  pool_id       = "${vkcs_lb_pool.pool.id}"
  subnet_id     = "${vkcs_networking_subnet.snet.id}"
}

resource "vkcs_networking_floatingip" "floatip_1" {
  pool    = "ext-net"
  port_id = "${vkcs_lb_loadbalancer.lb.vip_port_id}"
}

