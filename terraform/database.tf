data "vkcs_compute_flavor" "db" {
   name = "${var.db-instance-flavor}"
}

resource "vkcs_db_instance" "db-instance" {
  name        = "db-instance"

  availability_zone = "GZ1"

  datastore {
    type    = "mysql"
    version = "8.0"
  }
  keypair     = "${var.key_pairs.vm}" 
  flavor_id   = "${data.vkcs_compute_flavor.db.id}" 
  size        = 10
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }

  network {
    uuid = "${vkcs_networking_network.net.id}"
    fixed_ip_v4 = "10.10.10.15"
  }

  depends_on = [
    vkcs_networking_router_interface.db
  ]

  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port" : "9100"
    }
  }
}

resource "vkcs_db_database" "db-database" {
  name        = var.db_database
  dbms_id     = vkcs_db_instance.db-instance.id
  charset     = "utf8"
  collate     = "utf8_general_ci"
}

resource "vkcs_db_user" "db-user" {
  name        = var.db_username
  password    = var.db_user_password
  dbms_id = vkcs_db_instance.db-instance.id
  databases   = [vkcs_db_database.db-database.name]
}


resource "vkcs_db_backup" "db_1-backup" {
  name = "db_1-backup"
  dbms_id = vkcs_db_instance.db-instance.id
}